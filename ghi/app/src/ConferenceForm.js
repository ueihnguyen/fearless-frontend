import React from "react";

class ConferenceForm extends React.Component {
  constructor() {
    super();
    this.state = {
      locations: [],
      name: '',
      starts: '',
      ends: '',
      description: '',
      max_presentations: '',
      max_attendees: '',
      location: '',
    }
  }

  async componentDidMount() {
    const url = 'http://localhost:8000/api/locations/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      this.setState({
        locations: data.locations
      });
    }
  }

  handleInputChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value
    });
  }

  handleFormSubmit = async (e) => {
    e.preventDefault();
    let data = {...this.state};
    delete data.locations;
    const response = await fetch('http://localhost:8000/api/conferences/', {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json'
      },
    });
    if (response.ok) {
      const newConference = await response.json();
      console.log(newConference);
      this.setState({
        name: '',
        starts: '',
        ends: '',
        description: '',
        max_attendees: '',
        max_presentations: '',
        location: '',
      });
    }
  }

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new Conference</h1>
            <form onSubmit={this.handleFormSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input value={this.state.name} onChange={this.handleInputChange} placeholder="Name" required type="text" id="name" name="name" className="form-control"/>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input value={this.state.starts} onChange={this.handleInputChange} placeholder="Starts" required type="date" id="starts" name="starts" className="form-control"/>
                <label htmlFor="starts">Starts</label>
              </div>
              <div className="form-floating mb-3">
                <input value={this.state.ends} onChange={this.handleInputChange} type="date" id="ends" name="ends" className="form-control"/>
                <label htmlFor="ends">Ends</label>
              </div>
              <div className="mb-3">
                <label htmlFor="description" className="form-label">Description</label>
                <textarea value={this.state.description} onChange={this.handleInputChange} id="description" name="description" className="form-control" style={{height: "100px"}}></textarea>
              </div>
              <div className="form-floating mb-3">
                <input value={this.state.max_presentations} onChange={this.handleInputChange} placeholder="Maximum presentations" required type="number" id="max_presentations" name="max_presentations" className="form-control"/>
                <label htmlFor="max_presentations">Maximum presentations</label>
              </div>
              <div className="form-floating mb-3">
                <input value={this.state.max_attendees} onChange={this.handleInputChange} placeholder="Maximum attendees" required type="number" id="max_attendees" name="max_attendees" className="form-control"/>
                <label htmlFor="max_attendees">Maximum attendees</label>
              </div>
              <div className="mb-3">
                <select value={this.state.location} onChange={this.handleInputChange} required id="location" className="form-select" name="location">
                  <option>Choose a location</option>
                  {this.state.locations.map(location => {
                    return (
                      <option key={location.href} value={location.location_id}>{location.name}</option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default ConferenceForm;
