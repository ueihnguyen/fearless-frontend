import React, { useEffect, useState } from "react";

function PresentationForm() {

  const [conferences, setConferences] = useState([]);
  const [formInput, setFormInput] = useState({
    presenter_name: '',
    presenter_email: '',
    company_name: '',
    title: '',
    synopsis: '',
    conference: '',
  });

  useEffect(() => {
    fetch("http://localhost:8000/api/conferences/")
      .then(response => response.json())
      .then(data => setConferences(data.conferences))
  },[]);

  const handleInputChange = (e) => {
    setFormInput({
      ...formInput,
      [e.target.id]: e.target.value
    })
  }

  const clearState = () => {
    setFormInput({
      presenter_name: '',
      presenter_email: '',
      company_name: '',
      title: '',
      synopsis: '',
      conference: '',
    })
  }

  const handleFormSubmit = async (e) => {
    e.preventDefault();
    let data = {...formInput};
    delete data.conference;
    const response = await fetch(`http://localhost:8000/api/conferences/${formInput.conference}/presentations/`, {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    });
    if (response.ok) {
      const newPresentation = await response.json();
      console.log(newPresentation);
      clearState();
    }
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new presentation</h1>
          <form onSubmit={handleFormSubmit} id="create-presentation-form">
            <div className="form-floating mb-3">
              <input value={formInput.presenter_name} onChange={handleInputChange} placeholder="Name" required type="text" id="presenter_name" name="presenter_name" className="form-control"/>
              <label htmlFor="presenter_name">Presenter Name</label>
            </div>
            <div className="form-floating mb-3">
              <input value={formInput.presenter_email} onChange={handleInputChange} placeholder="presenter_email" required type="email" id="presenter_email" name="presenter_email" className="form-control"/>
              <label htmlFor="presenter_email">Presenter email</label>
            </div>
            <div className="form-floating mb-3">
              <input value={formInput.company_name} onChange={handleInputChange} placeholder="Company Name" type="text" id="company_name" name="company_name" className="form-control"/>
              <label htmlFor="company_name">Company name</label>
            </div>
            <div className="form-floating mb-3">
              <input value={formInput.title} onChange={handleInputChange} placeholder="Title" required type="text" id="title" name="title" className="form-control"/>
              <label htmlFor="title">Title</label>
            </div>
            <div className="mb-3">
              <label htmlFor="synopsis" className="form-label">Synopsis</label>
              <textarea value={formInput.synopsis} onChange={handleInputChange} id="synopsis" name="synopsis" className="form-control" style={{height: "100px"}}></textarea>
            </div>
            <div className="mb-3">
              <select value={formInput.conference} onChange={handleInputChange} required id="conference" className="form-select" name="conference">
                <option>Choose a conference</option>
                {conferences.map( conference => <option key={conference.href} value={conference.id}>{conference.name}</option>
                )}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default PresentationForm;
