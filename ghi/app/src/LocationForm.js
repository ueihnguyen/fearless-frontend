import React from 'react';

class LocationForm extends React.Component {
    constructor() {
        super();
        this.state = {
            states: [],
            name: '',
            room_count: '',
            city: '',
            state: '',
        };
    }

    async componentDidMount() {
        const url = 'http://localhost:8000/api/states/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            this.setState({
                states: data.states
            });
        }
    }

    handleInputChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        });
    }


    handleFormSubmit = async (e) => {
        e.preventDefault();
        let data = {...this.state};
        delete data.states;

        const response = await fetch('http://localhost:8000/api/locations/', {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        });
        if (response.ok) {
            const newLocation = await response.json();
            console.log(newLocation);
            this.setState({
                name: '',
                room_count: '',
                city: '',
                state: '',
            })
        }
    }


    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new location</h1>
                        <form id="create-location-form" onSubmit={this.handleFormSubmit}>
                            <div className="form-floating mb-3">
                                <input value={this.state.name} placeholder="Name" required type="text" onChange={this.handleInputChange} id="name" name="name" className="form-control"/>
                                <label htmlFor="name">Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={this.state.room_count} onChange={this.handleInputChange} placeholder="Room count" required type="number" id="room_count" name="room_count" className="form-control"/>
                                <label htmlFor="room_count">Room count</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={this.state.city} placeholder="City" required type="text" onChange={this.handleInputChange} id="city" name="city" className="form-control"/>
                                <label htmlFor="city">City</label>
                            </div>
                            <div className="mb-3">
                                <select value={this.state.state} required id="state" className="form-select" onChange={this.handleInputChange} name="state">
                                    <option>Choose a state</option>
                                    {this.state.states.map(state => {
                                        return (
                                            <option key={state.abbreviation} value={state.abbreviation}>{state.state}</option>
                                        );
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}


export default LocationForm;
