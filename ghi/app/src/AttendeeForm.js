import React from "react";
import logo from './logo.svg';

class AttendeeForm extends React.Component {

  constructor() {
    super();
    this.state = {
      conferences: [],
      success: false,
      name: '',
      conference: '',
      email: '',
    }
  }

  async componentDidMount() {
    const response = await fetch('http://localhost:8000/api/conferences/');
    if (response.ok) {
      const data = await response.json();
      this.setState({
        conferences: data.conferences
      });
    }
  }

  handleInputChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value
    });
  }

  handleFormSubmit = async (e) => {
    e.preventDefault();
    let data = {...this.state};
    delete data.conferences;
    delete data.success;
    const response = await fetch('http://localhost:8001/api/attendees/', {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json'
      },
    });
    if (response.ok) {
      const newAttendee = await response.json();
      console.log(newAttendee);
      this.setState({
        success: true,
        conference: '',
        name: '',
        email: '',
      });

    }
  }

  render() {
    let loadingClass = 'd-flex justify-content-center mb-3';
    let dropdownClass = 'form-select d-none';
    if (this.state.conferences.length > 0) {
      loadingClass = 'd-flex justify-content-center mb-3 d-none';
      dropdownClass = 'form-select';
    }
    let formClass = '';
    let alertClass = 'alert alert-success d-none mb-0';
    if (this.state.success) {
      formClass = 'd-none';
      alertClass = 'alert alert-success mb-0';
    }
    return (
      <div className="my-5 container">
        <div className="row">
          <div className="col col-sm-auto">
            <img width="300" className="bg-white rounded shadow d-block mx-auto mb-4" src={logo}/>
          </div>
          <div className="col">
            <div className="card shadow">
              <div className="card-body">
                <form onSubmit={this.handleFormSubmit} className={formClass} id="create-attendee-form">
                  <h1 className="card-title">It's Conference Time!</h1>
                  <p className="mb-3">
                    Please choose which conference
                    you'd like to attend.
                  </p>
                  <div className={loadingClass} id="loading-conference-spinner">
                    <div className="spinner-grow text-secondary" role="status">
                      <span className="visually-hidden">Loading...</span>
                    </div>
                  </div>
                  <div className="mb-3">
                    <select value={this.state.conference} onChange={this.handleInputChange} name="conference" id="conference" className={dropdownClass} required>
                      <option value="">Choose a conference</option>
                      {this.state.conferences.map(conf => {
                        return (
                          <option key={conf.href} value={conf.href}>{conf.name}</option>
                        );
                      })}
                    </select>
                  </div>
                  <p className="mb-3">
                    Now, tell us about yourself.
                  </p>
                  <div className="row">
                    <div className="col">
                      <div className="form-floating mb-3">
                        <input value={this.state.name} onChange={this.handleInputChange} required placeholder="Your full name" type="text" id="name" name="name" className="form-control"/>
                        <label htmlFor="name">Your full name</label>
                      </div>
                    </div>
                    <div className="col">
                      <div className="form-floating mb-3">
                        <input value={this.state.email} onChange={this.handleInputChange} required placeholder="Your email address" type="email" id="email" name="email" className="form-control"/>
                        <label htmlFor="email">Your email address</label>
                      </div>
                    </div>
                  </div>
                  <button className="btn btn-lg btn-primary">I'm going!</button>
                </form>
                <div className={alertClass} id="success-message">
                  Congratulations! You're all signed up!
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default AttendeeForm;
