
window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/states/';

    try {
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();

            for (let state of data.states) {

                const option = document.createElement("option");
                option.value = state.abbreviation;
                option.innerHTML = state.state;
                const selectTag = document.getElementById("state");
                selectTag.appendChild(option);
            }

        } else {
            throw new Error('Response is not ok');
        }

        const formTag = document.getElementById('create-location-form');
        formTag.addEventListener('submit', async (event) => {
            event.preventDefault();
            const formData = new FormData(formTag);
            const dataObject = Object.fromEntries(formData);

            const fetchConfig = {
                method: "post",
                body: JSON.stringify(dataObject),
                headers: {
                    "Content-Type": "application/json",
                },
            }

            const locationUrl = 'http://localhost:8000/api/locations/';
            const response = await fetch(locationUrl, fetchConfig);
            if (response.ok) {
                formTag.reset();
                const newLocation = await response.json();
                console.log(newLocation);
            }

        })

    } catch (e) {
        console.log("Error ----> " , e);
    }



});
