window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
        const selectTag = document.getElementById('conference');
        const response = await fetch(url);
        if (response.ok) {
        const data = await response.json();

        for (let conference of data.conferences) {
            const option = document.createElement('option');
            option.value = conference.href;
            option.innerHTML = conference.name;
            selectTag.appendChild(option);
        }
        } else {
            throw new Error('Response is not ok');
        }

        const spinTag = document.getElementById("loading-conference-spinner");
        spinTag.classList.add("d-none");
        selectTag.classList.remove("d-none");

        const formTag = document.getElementById("create-attendee-form");

        formTag.addEventListener('submit', async (event) => {
            event.preventDefault();
            const formData = new FormData(formTag);
            const dataObject = Object.fromEntries(formData);

            const fetchConfig = {
                method: 'post',
                body: JSON.stringify(dataObject),
                headers: {
                    'Content-Type': 'application/json',
                },
            }

            const attendeeURL = 'http://localhost:8001/api/attendees/';
            const response = await fetch(attendeeURL, fetchConfig);

            if (response.ok) {
                const alertTag = document.getElementById("success-message");
                alertTag.classList.remove('d-none');
                formTag.className = 'd-none';
            }

        });

    } catch (e) {
        console.log('Error ------------->', e);
    }
  });
