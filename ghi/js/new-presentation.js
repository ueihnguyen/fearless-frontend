window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
        const selectTag = document.getElementById('conference');
        const response = await fetch(url);
        if (response.ok) {
        const data = await response.json();

        for (let conference of data.conferences) {
            const option = document.createElement('option');
            option.value = conference.id;
            option.innerHTML = conference.name;
            selectTag.appendChild(option);
        }
        } else {
            throw new Error('Response is not ok');
        }

        const formTag = document.getElementById('create-presentation-form');
        formTag.addEventListener('submit', async (event) => {
            event.preventDefault();
            const formData = new FormData(formTag);
            const dataObject = Object.fromEntries(formData);

            const presentationUrl = `http://localhost:8000/api/conferences/${dataObject.conference}/presentations/`;

            delete dataObject.conference;

            const fetchConfig = {
                method: 'post',
                body: JSON.stringify(dataObject),
                headers: {
                    "Content-Type": "application/json",
                },
            }

            const response = await fetch(presentationUrl, fetchConfig);
            if (response.ok) {
                formTag.reset();
                const newPresentation = await response.json();
                console.log(newPresentation);
            }

        });

    } catch (e) {
        console.log('Error ------------->', e);
    }
  });
