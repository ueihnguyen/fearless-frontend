
window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/locations/';

    try {

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();

            for (let location of data.locations) {

                const option = document.createElement("option");
                option.value = location.location_id;
                option.innerHTML = location.name;
                const selectTag = document.getElementById("location");
                selectTag.appendChild(option);

            }

        } else {
            throw new Error('Response is not ok');
        }

        const formTag = document.getElementById('create-conference-form');
        formTag.addEventListener('submit', async (event) => {
            event.preventDefault();
            const formData = new FormData(formTag);
            const dataObject = Object.fromEntries(formData);
            console.log(dataObject);

            const fetchConfig = {
                method: 'post',
                body: JSON.stringify(dataObject),
                headers: {
                    "Content-Type": "application/json",
                },
            }

            const conferenceUrl = "http://localhost:8000/api/conferences/";
            const response = await fetch(conferenceUrl, fetchConfig);
            if (response.ok) {
                formTag.reset();
                const newConference = await response.json();
                console.log(newConference);
            }

        });

    } catch (e) {
        console.log('Error ------------->', e);
    }

});
