
function createCard(name, description, pictureUrl, start, end, location) {
    return `
        <div class="card shadow mt-3 mb-3">
            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body">
                <h5 class="card-title">${name}</h5>
                <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
                <p class="card-text">${description}</p>
            </div>
            <div class="card-footer">
                ${start} - ${end}
            </div>
        </div>
    `;
}

function createPlaceHolderCard() {
    return `
        <div class="card shadow mt-3 mb-3 placeholder-glow" aria-hidden="true">
            <img src="https://wallpaperaccess.com/full/2474837.jpg" class="card-img-top">
            <div class="card-body">
                <h5 class="card-title">
                    <span class="placeholder col-7"></span>
                </h5>
                <h6 class="card-subtitle mb-2 text-muted">
                    <span class="placeholder col-5"></span>
                </h6>
                <p class="card-text">
                    <span class="placeholder col-6"></span>
                    <span class="placeholder col-4"></span>
                    <span class="placeholder col-7"></span>
                    <span class="placeholder col-6"></span>
                    <span class="placeholder col-8"></span>
                </p>
            </div>
            <div class="card-footer">
                <span class="placeholder col-7"></span>
            </div>
        </div>
    `;
}

function createErrorCard(error) {
    return `
        <div class="card h-100 shadow bg-danger">
            <div class="card-body">
            <p class="alert alert-warning">${error}</p>
            </div>
        </div>
    `
}


window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);

        if (response.ok){
            const data = await response.json();

            for (let conference of data.conferences){

                const placeholderHtml = createPlaceHolderCard();
                const columns = document.querySelectorAll('.col');
                let currentIndex = data.conferences.indexOf(conference) % 3;
                columns[currentIndex].innerHTML += placeholderHtml;
            }

            for (let conference of data.conferences){

                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok){
                    const details = await detailResponse.json();
                    const name = details.conference.name;
                    const description = details.conference.description;
                    const picUrl = details.conference.location.picture_url;
                    const start = new Date(details.conference.starts).toLocaleDateString();
                    const end = new Date(details.conference.ends).toLocaleDateString();
                    const location = details.conference.location.name;
                    const html = createCard(name, description, picUrl, start, end, location);
                    const columns = document.querySelectorAll('.col');
                    let currentIndex = data.conferences.indexOf(conference) % 3;
                    columns[currentIndex].innerHTML = columns[currentIndex].innerHTML.replace(createPlaceHolderCard(), html);
                }
            }

        } else {
            throw new Error('Response not ok');
        }
    } catch (e) {
        const errorHtml = createErrorCard(e);
        const container = document.querySelector('.container');
        container.innerHTML = errorHtml;
    }


});
